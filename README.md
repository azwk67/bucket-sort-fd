# Bucket Sort - Frontend Development 2021/22

Bei diesem Projekt handelt es sich um eine Web-Basierte Visualisierung des Algorithmus Bucket  Sort.

## Mitarbeiter


- Patrick Walczynski
- Antony Zwickl

## Installation
Im Projektverzeichnis befindet sich ein "build"-Ordner (Kompilierte Version), welcher alle benötigten Assets und Libaries enthält so, dass keine Node-Module installiert werden müssen.

