window.addEventListener('load', function () {
    let mediaQuery = window.matchMedia('(max-width: 768px)')

    let startingTimeline = anime.timeline({
        autoplay: false
    })
    startingTimeline
        .add({
            targets: document.getElementById('back'),
            translateY: [200,0],
            opacity:[0,1]
        },200)
        .add({
            targets: document.getElementById('submitToAlgo'),
            translateY: [300,0],
            opacity:[0,1],
        },500)
        .add({
            targets: document.getElementById('heading'),
            translateY: [-300,0],
            opacity:[0,1]
        },600)
        .add({
            targets: document.getElementById('valueInput'),
            width: mediaQuery.matches ? [0,'40vw'] : [0,'20vw'],
            opacity: [0,1],
            duration: 1500
        },1000)
        .add({
            targets: document.getElementById('labelInput'),
            translateY: [200,0],
            opacity:[0,1]
        },300)
    startingTimeline.play()
})



