let numberOfElements = localStorage.getItem('number')
let elements = []
let dotContainer = document.getElementById('dotContainer')
let boxContainer = document.getElementById('boxContainer')
let timeline = anime.timeline()
let scaleDownTimeline = anime.timeline()
let scaleUpTimeline = anime.timeline({delay: 100})
let sortedArray = []
let squareRootOfElements = Math.round(Math.sqrt(numberOfElements))
let minValue = 100
let maxValue = 0
let allBuckets;
let bucketSize;
let boxes;
let bucketCount;
let stepText =  document.getElementById('stepText')
let playButton  = document.getElementById('play')
let pauseButton =  document.getElementById('pause')
let speedMultiplicator = 1
let speedSlider = document.getElementById('speedSlider')

speedSlider.addEventListener('change', () =>  {
    let value = speedSlider.value
    switch (value){
        case "1": speedMultiplicator = 2; break;
        case "2": speedMultiplicator = 1.5; break;
        case "3": speedMultiplicator = 1; break;
        case "4": speedMultiplicator = 0.75; break;
        case "5": speedMultiplicator = 0.5; break;
    }
    console.log(value)
    console.log(speedMultiplicator)
})

stepText.innerHTML = 'Generierung'

let currentTL = anime.timeline()

const handleTimelinePause = () => currentTL.pause()
const handleTimelinePlay = () => currentTL.play()

for (let i = 0; i < numberOfElements; i++) {
    let temp = (Math.floor(Math.random() * 50)) + 31
    if (!elements.includes(temp)) {
        elements.push(temp)
    } else {
        i--
    }
}

const createElements = (number) => {
    for (let i = 0; i < number; i++) {
        let width = 1 * elements[i]
        let height = 1 * elements[i]
        dotContainer.innerHTML += '<span class="dot" style="width:' + width + 'px; height: ' + height + 'px; " >' + elements[i] + '</span>'
    }
    let dots = document.getElementsByClassName('dot')
    let creatingTimeline = anime.timeline()
    creatingTimeline
        .add({
                targets: dots,
                translateX: [-100, 0],
                opacity: [0, 1],
                delay: anime.stagger(500 * speedMultiplicator) // increase delay by 100ms for each elements.
            }
        )
    creatingTimeline.finished.then(() => createBuckets())

    currentTL = creatingTimeline

    pauseButton.addEventListener('click', handleTimelinePause)
    playButton.addEventListener('click', handleTimelinePlay)
}

const createBuckets = () => {
    elements.forEach(function (currentVal) {
        if (currentVal < minValue) {
            minValue = currentVal;
        } else if (currentVal > maxValue) {
            maxValue = currentVal;
        }
    })

    bucketSize = Math.ceil((maxValue - minValue) / squareRootOfElements)
    // Initializing buckets
    bucketCount = Math.floor((maxValue - minValue) / bucketSize) + 1;
    allBuckets = new Array(bucketCount);
    for (let i = 0; i < allBuckets.length; i++) {
        allBuckets[i] = [];
        boxContainer.innerHTML += '<div class="singleBox"><div class="box" id="' + (i + 1) + '"></div><p>' + (i + 1) + '</p></div>'
    }

    currentTL = timeline

    boxes = Array.from(document.getElementsByClassName('box'))
    timeline.add({
        targets: boxes,
        translateY: [200, 0],
        opacity: [0, 1],
        rotate: [-50, 0],
        delay: anime.stagger(500 * speedMultiplicator)
    }).finished.then(() => pushToBucket())
}

const pushToBucket = () => {
    stepText.innerHTML = 'Zuordnung'
    let dots = Array.from(document.getElementsByClassName('dot'))
    elements.forEach(function (currentVal) {
        if(allBuckets[Math.floor((currentVal - minValue) / bucketSize)] === undefined) {
            allBuckets[bucketCount - 1].push(currentVal);
        }
        else {
            allBuckets[Math.floor((currentVal - minValue) / bucketSize)].push(currentVal);
        }
        let circleToMove = dots.find((element) => Number(element.innerHTML) === currentVal)
        currentTL = scaleDownTimeline
        scaleDownTimeline.add({
            targets: circleToMove,
            scale: 0,
            duration: 1000 * speedMultiplicator
        })
        scaleDownTimeline.finished.then(() => {
            let boxToPutIn = boxes.find(box => Number(box.id) === helperBorder(currentVal))
            circleToMove.className = `dot dotsInBox${boxToPutIn.id}`
            circleToMove.remove()
            boxToPutIn.appendChild(circleToMove)
            currentTL = scaleUpTimeline
            scaleUpTimeline.add({
                targets: circleToMove,
                scale: 1,
                duration: 1000 * speedMultiplicator
            })
        })
    });
    scaleUpTimeline.finished.then(() => sortInBuckets())
}

const sortInBuckets = () => {
    currentTL = null
    stepText.innerHTML = 'Sortierung'
    elements.length = 0;
    allBuckets.forEach(function (bucket, index) {
        insertionSort(bucket, index + 1);
        bucket.forEach(function (element) {
            elements.push(element)
        });
    });
}

createElements(numberOfElements)

const sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const insertionSort = async (array, index) => {
    let length = array.length;
    let dots = Array.from(document.getElementsByClassName(`dotsInBox${index}`))
    let currentBox = document.getElementById(index)

    if(length === 1 || length === 0){
        if(sortedArray.length === 0 || sortedArray[length-1].innerHTML < dots[0].innerHTML) sortedArray.push(...dots)  //FIX: Unsorted
        else if(sortedArray.find(number => number < dots[0])){
            let tempArray = sortedArray.filter(number => number.innerHTML < dots[0].innerHTML)
            let tempIndex = sortedArray.indexOf(tempArray[tempArray.length-1])
            sortedArray.splice(tempIndex+1,0,...dots)
        }
        else sortedArray.unshift(...dots)
        console.log(sortedArray)
        currentBox.style.backgroundColor = '#4d4d4d'
        if(boxes.every(box => box.style.backgroundColor === 'rgb(77, 77, 77)')){
            moveDots()
        }
    }

    for (let i = 1; i < length; i++) {
        let temp = array[i];
        let circle1 = dots[i] //Rot färben
        for (var j = i - 1; j >= 0 && array[j] > temp; j--) {
            let circle2 = dots[j]
            await sleep(2000 * speedMultiplicator)
            let tempTimeline = anime.timeline()
            tempTimeline.add({
                targets: circle2,
                scale: 0,
                duration: 1000 * speedMultiplicator
            })
            tempTimeline.finished.then(() =>  {
                circle1.after(circle2)
                dots[j+1] = dots[j]
                array[j + 1] = array[j];
                let tempTimeline2 = anime.timeline()
                tempTimeline2
                    .add({
                        targets: circle2,
                        scale: 1,
                        duration: 1000 * speedMultiplicator
                    })
            })
            await sleep(2000 * speedMultiplicator)
        }
        dots[j+1] = circle1
        array[j + 1] = temp;
        if(i === length-1) {
            if(sortedArray.length === 0 || sortedArray[sortedArray.length-1].innerHTML < dots[0].innerHTML) sortedArray.push(...dots)
            else if(sortedArray.find(number => number.innerHTML < dots[0].innerHTML)){
                console.log('this will be unsorted')
                let tempArray = sortedArray.filter(number => number.innerHTML < dots[0].innerHTML)
                let tempIndex = sortedArray.indexOf(tempArray[tempArray.length-1])
                console.log(tempIndex)
                console.log(tempArray)
                sortedArray.splice(tempIndex+1,0,...dots)
            }
            else sortedArray.unshift(...dots)
            console.log(sortedArray)
            currentBox.style.backgroundColor = '#4d4d4d'
            if(boxes.every(box => box.style.backgroundColor === 'rgb(77, 77, 77)')){
                moveDots()
            }
        }
    }
}

const helperBorder = (number) => {
    let tempVal = Math.floor((number - minValue) / bucketSize) + 1
    return tempVal > bucketCount ? bucketCount : tempVal
}

const moveDots = () => {
    stepText.innerHTML = 'Zusammenfügen'
    let dots = document.getElementsByClassName('dot')
    let length = dots.length

    let tempTimeline1 = anime.timeline()
    tempTimeline1
        .add({
            targets: dots,
            scale: 0,
            delay: anime.stagger(200 * speedMultiplicator)
        })
    currentTL = tempTimeline1

    tempTimeline1.finished.then(()  => {
        playButton.remove()
        pauseButton.remove()
        document.getElementById('speedController').remove()
        for(let i = 0; i < length; i++){
            dots[0].remove()
        }
        let tempTimeline2 = anime.timeline()
        tempTimeline2.add({
            targets: [document.getElementById('boxContainer'), document.getElementById('dotContainer'), document.getElementsByClassName('singleBox')],
            height: 0,
            opacity: 0,
            duration: 1000 * speedMultiplicator
        })
        tempTimeline2.finished.then(()  => {
            let singleBoxes = document.getElementsByClassName('singleBox')
            let length =  singleBoxes.length
            for(let i = 0; i<length; i++){
                singleBoxes[0].remove()
            }
        })

        let container = document.getElementById('finalContainer')
        sortedArray.forEach(dot => {
            container.appendChild(dot)
        })
        anime({
            targets: sortedArray,
            scale: 1,
            delay: anime.stagger(200 * speedMultiplicator)
        }).finished.then(() => {
            stepText.innerHTML = 'Fertig'
        })
    })
}

