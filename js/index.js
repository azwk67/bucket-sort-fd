window.addEventListener('load', function () {  //Load-Animation
    const pfeilLinksSVG = document.getElementById('pfeil_links')
    let svgLinks = pfeilLinksSVG.contentDocument.rootElement
    svgLinks.style.cursor = 'pointer'
    svgLinks.addEventListener('click', () => window.location.href='../html/info.html')
        const pfeilLinksHinten = pfeilLinksSVG.contentDocument.getElementById('l_hinten')
        const pfeilLinksMitte = pfeilLinksSVG.contentDocument.getElementById('l_mitte')
        const pfeilLinksVorne = pfeilLinksSVG.contentDocument.getElementById('l_vorne')

    const pfeilRechtsSVG = document.getElementById('pfeil_rechts')
    let svgRechts = pfeilRechtsSVG.contentDocument.rootElement
    svgRechts.style.cursor = 'pointer'
    svgRechts.addEventListener('click', () => window.location.href='../html/configurator.html')
        const pfeilRechtsHinten = pfeilRechtsSVG.contentDocument.getElementById('r_hinten')
        const pfeilRechtsMitte = pfeilRechtsSVG.contentDocument.getElementById('r_mitte')
        const pfeilRechtsVorne = pfeilRechtsSVG.contentDocument.getElementById('r_vorne')

    let startTimeline = anime.timeline({
        autoplay: false,
    })
    startTimeline
        .add({
            targets: document.getElementById('bucket'),
            opacity: [0, 1],
            translateY: [-500, 0]
        })
        .add({
            targets: document.getElementById('mainTitle'),
            opacity: [0, 1],
            translateX: [300, 0]

        })
        .add({
            targets: document.getElementById('infoButton'),
            opacity: [0, 1],
            translateX: [-200, 0]
        }, 300)
        .add({
            targets: document.getElementById('startButton'),
            opacity: [0, 1],
            translateX: [200, 0]
        }, 500)
        .add({
            targets: [pfeilLinksHinten, pfeilLinksMitte, pfeilLinksVorne],
            opacity: [0,1],
            translateX:[100,0],
            delay: anime.stagger(100)
        },700)
        .add({
            targets: [pfeilRechtsHinten, pfeilRechtsMitte, pfeilRechtsVorne],
            opacity: [0,1],
            translateX:[-100,0],
            delay: anime.stagger(100)
        },700)
    startTimeline.play()
    startTimeline.finished.then(() => {
        anime({
            targets: [pfeilLinksHinten, pfeilLinksMitte, pfeilLinksVorne],
            direction: 'alternate',
            loop: true,
            translateX:[20,0],
            delay: anime.stagger(200)
        })
        anime({
            targets: [pfeilRechtsHinten, pfeilRechtsMitte, pfeilRechtsVorne],
            direction: 'alternate',
            loop: true,
            translateX:[-20,0],
            delay: anime.stagger(200)
        })
    })
})

let el = document.getElementsByClassName('tilt')[0]
const height = el.clientHeight
const width = el.clientWidth

el.addEventListener('mousemove', handleMove)

function handleMove(e) {
    const xVal = e.layerX
    const yVal = e.layerY
    const yRotation = 20 * ((xVal - width / 2) / width)
    const xRotation = -20 * ((yVal - height / 2) / height)
    el.style.transform = 'perspective(500px) scale(1.1) rotateX(' + xRotation + 'deg) rotateY(' + yRotation + 'deg)'
}

el.addEventListener('mouseout', function() {
    el.style.transform = 'perspective(500px) scale(1) rotateX(0) rotateY(0)'
})
el.addEventListener('mousedown', function() {
    el.style.transform = 'perspective(500px) scale(0.9) rotateX(0) rotateY(0)'
})
el.addEventListener('mouseup', function() {
    el.style.transform = 'perspective(500px) scale(1.1) rotateX(0) rotateY(0)'
})

const infoButton = document.getElementById('infoButton')
infoButton.addEventListener('mouseover',() => {
    anime({
        targets:infoButton,
        translateX:-10
    })
})
infoButton.addEventListener('mouseleave',() => {
    anime({
        targets:infoButton,
        translateX:10
    })
})

const startButton = document.getElementById('startButton')
startButton.addEventListener('mouseover',() => {
    anime({
        targets:startButton,
        translateX:10
    })
})
startButton.addEventListener('mouseleave',() => {
    anime({
        targets:startButton,
        translateX:-10
    })
})

anime({ //Button-Wiggle Animation
    targets: "#startButton",
    delay: 5000,
    loop: true,
    keyframes:[
        {translateX: 5},
        {translateX: -5},
        {translateX: 5},
        {translateX: -5},
    ],
    duration: 1000,
    direction: 'alternate',
    easing: 'easeInBack'
})

