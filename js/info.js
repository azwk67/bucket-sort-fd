let slide0, slide1, slide2, slide3, slide4, slide5 = false

let backButton = document.getElementById('backSVG')
backButton.addEventListener('click',() => window.location.href='../html/index.html')

let scrollDownHinweis = anime({
    targets: '#scrollDown',
    opacity: [0,1],
    loop: true,
    duration: 2000,
    delay: 3000,
    direction: 'alternate',
    translateY: 10,
    autoplay: false
})
let myFullpage = new fullpage('#fullpage', { //FullPage.js für die Folien
    //Navigation
    menu: '#menu',
    lockAnchors: false,
    anchors:['firstPage', 'secondPage'],
    navigation: true,
    navigationPosition: 'left',
    navigationTooltips: ['Start', 'Bucket Sort?','Generierung', 'Zuordnung','Sortierung','Zusammenfügung','Vorteile'],
    showActiveTooltip: false,
    slidesNavigation: false,
    slidesNavPosition: 'bottom',

    //Scrolling
    css3: true,
    scrollingSpeed: 1500,
    autoScrolling: true,
    fitToSection: true,
    fitToSectionDelay: 1000,
    scrollBar: false,
    easing: 'easeInOutCubic',
    easingcss3: 'ease',
    loopBottom: false,
    loopTop: false,
    loopHorizontal: true,
    continuousVertical: false,
    continuousHorizontal: false,
    scrollHorizontally: false,
    interlockedSlides: false,
    dragAndMove: false,
    offsetSections: false,
    resetSliders: false,
    fadingEffect: false,
    normalScrollElements: '#element1, .element2',
    scrollOverflow: false,
    scrollOverflowReset: false,
    scrollOverflowOptions: null,
    touchSensitivity: 15,
    bigSectionsDestination: null,

    //Accessibility
    keyboardScrolling: true,
    animateAnchor: true,
    recordHistory: true,

    //Design
    controlArrows: true,
    verticalCentered: true,
    paddingTop: '3em',
    paddingBottom: '10px',
    fixedElements: '#header, .footer',
    responsiveWidth: 769,
    responsiveHeight: 0,
    responsiveSlides: true,
    parallax: false,
    parallaxOptions: {type: 'reveal', percentage: 62, property: 'translate'},
    dropEffect: false,
    dropEffectOptions: { speed: 2300, color: '#F82F4D', zIndex: 9999},
    waterEffect: false,
    waterEffectOptions: { animateContent: true, animateOnMouseMove: true},
    cards: false,
    cardsOptions: {perspective: 100, fadeContent: true, fadeBackground: true},

    //Custom selectors
    sectionSelector: '.section',
    slideSelector: '.slide',

    lazyLoading: true,

    //events
    onLeave: function(origin, destination, direction){
        if(destination.index === 1 && !slide1){
            slide1 = true
            let waypoint = new Waypoint({
                element: document.getElementById('step0'),
                handler: ()  => {
                    anime({
                        targets: document.getElementById('step0'),
                        translateY: [400,0],
                        opacity: [0,1],
                        delay: 500,
                        duration:  3000,
                    })
                },
                context: document.getElementsByClassName('section')[1],
                offset: '100%'
            })
        }
        if(destination.index === 2 && !slide2){
            slide2 = true
            let waypoint = new Waypoint({
                element:  document.getElementById('step1'),
                handler: () => {
                    let textWrapper = document.querySelector('#heading1');
                    textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

                    let startTimeline = anime.timeline({
                        autostart: false
                    })
                    startTimeline
                        .add({
                            targets: document.getElementById('anim1'),
                            translateX: [300,0],
                            opacity: [0,1],
                            delay: 1000,
                            duration: 2000
                        })
                        .add({
                            targets: '#textContainer1 .letter',
                            opacity: [0,1],
                            scale: [0,1],
                            delay: anime.stagger(100)
                        },500)
                        .add({
                            targets: '#description1',
                            opacity:[0,1],
                            translateX: [-500,0],
                            duration: 2000
                        },2000)
                    startTimeline.play()
                    startTimeline.finished.then(() => {
                        let animationTimeline = anime.timeline({autostart:false})
                        animationTimeline
                            .add({
                                targets: document.getElementsByClassName('dots1'),
                                opacity:[0,1],
                                delay: anime.stagger(500),
                                scale: [0,1]
                            })
                            .add({
                                targets: document.getElementsByClassName('bucket1'),
                                opacity:[0,1],
                                delay: anime.stagger(500),
                                translateY: [200,0],
                                duration: 2000,
                                complete: () => {
                                    document.getElementById('anim1').addEventListener('click', () => {
                                        animationTimeline.play()
                                    })
                                }
                            },1000)
                        animationTimeline.play()
                        animationTimeline.finished.then(() => {
                            scrollDownHinweis.play()
                        })
                    })
                },
                context: document.getElementsByClassName('section')[2],
                offset: '100%'
            })
        }
        if(destination.index === 3 && !slide3){
            slide3 = true
            let waypoint = new Waypoint({
                element:  document.getElementById('step2'),
                handler: () => {
                    let textWrapper = document.querySelector('#heading2');
                    textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

                    let startTimeline = anime.timeline({
                        autostart: false
                    })
                    startTimeline
                        .add({
                            targets: document.getElementById('anim2'),
                            translateX: [-300,0],
                            opacity: [0,1],
                            delay: 1000,
                            duration: 2000
                        })
                        .add({
                            targets: '#textContainer2 .letter',
                            opacity: [0,1],
                            scale: [0,1],
                            delay: anime.stagger(100)
                        },500)
                        .add({
                            targets: '#description2',
                            opacity:[0,1],
                            translateY: [-500,0],
                            duration: 2000
                        },2000)
                    startTimeline.play()
                    startTimeline.finished.then(() => {
                        let dots = document.getElementsByClassName('dots2')
                        let boxes = document.getElementsByClassName('bucket2')
                        let temp = [...dots]

                        let animationTimeline = anime.timeline({
                            autostart:false
                        })
                        animationTimeline
                            .add({
                                targets: dots,
                                translateY: '-4.9vh'
                            })
                            .add({
                                targets: dots[0],
                                translateY:'17vh',
                                translateX: '15vw'
                            })
                            .add({
                                targets: dots[1],
                                translateY:'17vh',
                                translateX: '15vw'
                            })
                            .add({
                                targets: dots[2],
                                translateY:'17vh',
                                translateX: '-14vw'
                            })
                            .add({
                                targets: dots[3],
                                translateY:'16vh',
                                translateX: '-14vw'
                            })
                            .add({
                                targets: dots[4],
                                translateY:'17vh',
                                translateX: '-14vw',
                                complete: () => {
                                    document.getElementById('anim2').addEventListener('click', () => {
                                        animationTimeline.play()
                                    })
                                }
                            })
                    })
                },
                context: document.getElementsByClassName('section')[3],
                offset: '100%'
            })
        }
        if(destination.index === 4 && !slide4){
            slide4 = true
            let waypoint = new Waypoint({
                element:  document.getElementById('step3'),
                handler: () => {
                    let textWrapper = document.querySelector('#heading3');
                    textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

                    let startTimeline = anime.timeline({
                        autostart: false
                    })
                    startTimeline
                        .add({
                            targets: document.getElementById('anim3'),
                            translateX: [300,0],
                            opacity: [0,1],
                            delay: 1000,
                            duration: 2000
                        })
                        .add({
                            targets: '#textContainer3 .letter',
                            opacity: [0,1],
                            scale: [0,1],
                            delay: anime.stagger(100)
                        },500)
                        .add({
                            targets: '#description3',
                            opacity:[0,1],
                            translateX: [-500,0],
                            duration: 2000
                        },2000)
                    startTimeline.play()
                    startTimeline.finished.then(()=>{
                        let dots = document.getElementsByClassName('dots3')
                        let boxes = document.getElementsByClassName('bucket3')
                        let temp = [...dots]

                        let animationTimeline = anime.timeline()
                        animationTimeline
                            .add({
                                targets: dots[0],
                                translateX: '3vw'
                            },400)
                            .add({
                                targets: dots[1],
                                translateX: '-4vw'
                            },400)
                            .add({
                                targets: dots[3],
                                translateX: '5vw'
                            },1500)
                            .add({
                                targets: dots[4],
                                translateX: '-6vw',
                                complete: () => {
                                    document.getElementById('anim3').addEventListener('click', () => {
                                        animationTimeline.play()
                                    })
                                }
                            },1500)
                    })
                },
                context: document.getElementsByClassName('section')[4],
                offset: '100%'
            })
        }
        if(destination.index === 5 && !slide5){
            slide5 = true
            let waypoint = new Waypoint({
                element:  document.getElementById('step4'),
                handler: () => {
                    let textWrapper = document.querySelector('#heading4');
                    textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

                    let startTimeline = anime.timeline({
                        autostart: false
                    })
                    startTimeline
                        .add({
                            targets: document.getElementById('anim4'),
                            translateX: [-300,0],
                            opacity: [0,1],
                            delay: 1000,
                            duration: 2000
                        })
                        .add({
                            targets: '#textContainer4 .letter',
                            opacity: [0,1],
                            scale: [0,1],
                            delay: anime.stagger(100)
                        },500)
                        .add({
                            targets: '#description4',
                            opacity:[0,1],
                            translateY: [-500,0],
                            duration: 2000
                        },2000)
                    startTimeline.play()
                    startTimeline.finished.then(()=>{
                        let dots = document.getElementsByClassName('dots4')
                        let animationTimeline = anime.timeline({autostart:false})
                        animationTimeline
                            .add({
                                targets: document.getElementsByClassName('bucket4'),
                                translateY:'-15vh'
                            })
                            .add({
                                targets:dots,
                                translateY: '40vh',
                                delay: anime.stagger(400)
                            },2000)
                            .add({
                                targets:[dots[0],dots[1],dots[2]],
                                translateX: ' 2.9vw'
                            },5000)
                            .add({
                                targets:[dots[3],dots[4]],
                                translateX: '-2.9vw',
                                complete: () => {
                                    document.getElementById('anim4').addEventListener('click', () => {
                                        animationTimeline.play()
                                    })
                                }
                            },5000)
                    })
                },
                context: document.getElementsByClassName('section')[5],
                offset: '100%'
            })
        }
        if(destination.index === 6){
            scrollDownHinweis.pause()
            anime({
                targets: '#scrollDown',
                opacity: 0
            })
        }
    },
    afterLoad: function(origin, destination, direction){
        if(destination.index === 0 && !slide0){
            slide0 = true
            anime({
                targets: [document.getElementById('paragraphScroll') ,document.getElementById('mainHeading')],
                translateY: [300,0],
                opacity:[0,1],
                delay: anime.stagger(300)
            })
        }
    },
    afterRender: function(){},
    afterResize: function(width, height){},
    afterReBuild: function(){},
    afterResponsive: function(isResponsive){},
    afterSlideLoad: function(section, origin, destination, direction){},
    onSlideLeave: function(section, origin, destination, direction){}
});

let rakete = document.getElementById('rakete_body')
let feuer = document.getElementsByClassName('flamme')
let vorteilCards = document.getElementsByClassName('card')

vorteilCards[0].addEventListener('mouseenter', () => {
    anime({
        targets:rakete,
        translateX: 1,
        translateY: -1
    })
    anime({
        targets:[feuer[1],feuer[0],feuer[2]],
        opacity:1,
        delay:anime.stagger(50)
    })
})
vorteilCards[0].addEventListener('mouseleave', () => {
    anime({
        targets:rakete,
        translateX: -1,
        translateY: 1,
    })
    anime({
        targets:[feuer[1],feuer[0],feuer[2]],
        opacity:0,
        delay:anime.stagger(50),
    })
})
vorteilCards[1].addEventListener('mouseenter',() => {
    anime({
        targets:'#parallel path',
        duration:2500,
        strokeDashoffset: [anime.setDashoffset, 0],
    })
})
vorteilCards[1].addEventListener('mouseleave',() => {
    anime({
        targets:'#parallel path',
        duration:2500,
    })
})